import numpy as np
def gauss(x,a=1,b=0,c=1):
    """This is a gaussian function that takes in four integer parameters, 
    x = user input
    a = 1
    b = 0
    c = 1
    """

    #numerator = -(x-b)*(x-b)
    #denominator = 2*c*c
    #exponent = -(x-b)*(x-b)/2*c*c
    z = a*np.exp(-(x-b)*(x-b)/(2*c*c))
    return z

