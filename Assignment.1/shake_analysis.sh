chmod +x shake_analysis.sh
#how many lines are in file
awk 'END {print NR}' shakespeare-macbeth-46.txt
#how many lines contain LADY MACBETH
awk '/LADY MACBETH/{count++} END{print count}' shakespeare-macbeth-46.txt
#how many lines contain both LADY MACBETH and must
awk '/LADY MACBETH/&&/must/{count++} END{print count}' shakespeare-macbeth-46.txt
#prints line containing strings
#awk '/LADY MACBETH/&&/blood/' shakespeare-macbeth-46.txt
#print 8th word from line containing LADY MACBETH and blood
awk '/LADY MACBETH/&&/blood/ {print $8}' shakespeare-macbeth-46.txt
#replace all LADY MACBETH with LADY GAGA into file named shake.txt
sed 's/LADY MACBETH/LADY GAGA/g' shakespeare-macbeth-46.txt > shake.txt
