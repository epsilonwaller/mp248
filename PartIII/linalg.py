def dot_prod(u,v):
    '''
    vector product
    
    input: two vectors of equal lenght
    output: scalar 
        
    '''
    if len(u) is not len(v): 
        print("Error: vectors do not have same lenght.")
    return sum(u*v)